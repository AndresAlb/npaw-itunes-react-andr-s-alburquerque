A search bar to find music in iTunes
● A list of results in a grid format, where each result is an album (the search may return
results matching anything including the artists, album etc., but the entity displayed as
each result should be an album.)
● Each search result should be a card containing artwork, the artist name and the album
name.
● Show only the top 20 results

Bonus:
● A button to be able to switch between the grid format and a vertical list format for the
search results.
● Show more than 20 results, but only up to 20 results per page. Add a paging component
to switch between pages of results.